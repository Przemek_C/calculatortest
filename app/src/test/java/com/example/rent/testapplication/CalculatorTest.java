package com.example.rent.testapplication;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by RENT on 2017-08-05.
 */
public class CalculatorTest {
    Calculator calculator;
    @Before
    public void setUp() throws Exception {
        calculator = new Calculator();
    }

    @After
    public void tearDown() throws Exception {
        calculator = null;
    }

    @Test
    public void add() throws Exception {
        assertEquals(new BigDecimal(5), calculator.add(new BigDecimal(2), new BigDecimal(3)));

    }
    @Test
    public void min() throws Exception {
        assertEquals(new BigDecimal(-1), calculator.sub(new BigDecimal(2), new BigDecimal(3)));
    }
    @Test
    public void mul() throws Exception{
        assertEquals(new BigDecimal(Double.valueOf(10)), calculator.multi(new BigDecimal(2), new BigDecimal(5)));
    }
    @Test
    public void div() throws Exception{
        assertEquals(new BigDecimal(10), calculator.div(new BigDecimal(20), new BigDecimal(2)));
    }
}