package com.example.rent.testapplication;

import java.math.BigDecimal;

/**
 * Created by dariusznowak on 05.08.2017.
 */

public class Waluta {
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public BigDecimal getWartosc() {
        return wartosc;
    }

    public void setWartosc(BigDecimal wartosc) {
        this.wartosc = wartosc;
    }

    String nazwa;
    BigDecimal wartosc;

    public Waluta(String nazwa, BigDecimal wartosc) {
        this.nazwa = nazwa;
        this.wartosc = wartosc;
    }
}
