package com.example.rent.testapplication;

import java.math.BigDecimal;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
public class MainActivity extends AppCompatActivity {

    Calculator calculator;
    WalutyKonwerter walutyKonwerter;

    @BindView(R.id.score) TextView score;
    @BindView(R.id.num1) EditText num1;
    @BindView(R.id.num2) EditText num2;
    @BindView(R.id.text) TextView text;


    @OnClick(R.id.add)
    public void btnAdd(){
        text.setText("+");
        String a = num1.getText().toString();
        String b = num2.getText().toString();
        BigDecimal a1 = new BigDecimal(a);
        BigDecimal b1 = new BigDecimal(b);
        BigDecimal s = calculator.add(a1, b1);

        score.setText(String.valueOf(s));
    }

    @OnClick(R.id.min)
    public void btnMin(){
        text.setText("-");
        String a = num1.getText().toString();
        String b = num2.getText().toString();
        BigDecimal a1 = new BigDecimal(a);
        BigDecimal b1 = new BigDecimal(b);
        BigDecimal s = calculator.sub(a1, b1);

        score.setText(String.valueOf(s));
    }

    @OnClick(R.id.multi)
    public void btnMulti(){
        text.setText("*");
        String a = num1.getText().toString();
        String b = num2.getText().toString();
        BigDecimal a1 = new BigDecimal(a);
        BigDecimal b1 = new BigDecimal(b);
        BigDecimal s = calculator.multi(a1, b1);

        score.setText(String.valueOf(s));
    }

    @OnClick(R.id.div)
    public void btnDiv(){
        text.setText("/");
        String a = num1.getText().toString();
        String b = num2.getText().toString();
        BigDecimal a1 = new BigDecimal(a);
        BigDecimal b1 = new BigDecimal(b);
        BigDecimal s = calculator.div(a1, b1);

        score.setText(String.valueOf(s));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        calculator = new Calculator();
        walutyKonwerter = new WalutyKonwerter(new BigDecimal("3.99"));
    }

    @OnClick(R.id.currency)
    public void btnCurrency(){
        BigDecimal wartosc = walutyKonwerter.przeliczWalute(new BigDecimal(score.getText().toString()));
        Toast.makeText(getApplicationContext(), wartosc.toString(), Toast.LENGTH_LONG).show();
    }

}
