package com.example.rent.testapplication;

import java.math.BigDecimal;

/**
 * Created by RENT on 2017-08-05.
 */

public class Calculator {

    public BigDecimal add(BigDecimal a, BigDecimal b){
        return a.add(b);
    }
    public BigDecimal sub(BigDecimal a, BigDecimal b){
        return a.subtract(b);
    }
    public BigDecimal multi(BigDecimal a, BigDecimal b){
        return a.multiply(b);
    }
    public BigDecimal div(BigDecimal a, BigDecimal b){
        return a.divide(b);
    }
}
