package com.example.rent.testapplication;

import java.math.BigDecimal;

/**
 * Created by RENT on 2017-08-05.
 */

public class Currency {

    BigDecimal carrencyValue;

    public Currency(BigDecimal carrencyValue) {
        this.carrencyValue = carrencyValue;
    }

    public void setCarrencyValue(String carrency){
        carrencyValue = new BigDecimal(carrency);
    }

    public BigDecimal countCurrency(BigDecimal valueToCount){
        return valueToCount.multiply(carrencyValue);
    }
}
