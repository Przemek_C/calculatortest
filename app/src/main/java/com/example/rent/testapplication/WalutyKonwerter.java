package com.example.rent.testapplication;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dariusznowak on 05.08.2017.
 */

public class WalutyKonwerter {

    BigDecimal wartoscWaluty;
    List<Waluta> listaWalut;

    public  WalutyKonwerter(BigDecimal waluta){
        wartoscWaluty = waluta;
        listaWalut = new ArrayList<>();
    }


    public void ustawWalute(String waluta){
        wartoscWaluty = new BigDecimal(waluta);
    }

    public BigDecimal przeliczWalute(BigDecimal wartoscDoPrzeliczenia){
        return wartoscDoPrzeliczenia.multiply(wartoscWaluty);
    }

    // dodac klase Waluta
    // dodac liste Walut
    // dodac funkcje ktora dodaje waluty
    // napisac test powinienZrocPustaListeWalut

    public void AddWaluta(Waluta waluta){
        listaWalut.add(waluta);
    }

    public Waluta[] konwertujTabele(){
        Waluta[] tabelaWalut = new Waluta[listaWalut.size()];
        tabelaWalut = listaWalut.toArray(tabelaWalut);
        return tabelaWalut;
    }


}
