package com.example.rent.testapplication;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.robotium.solo.Solo;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by RENT on 2017-08-05.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    private Solo solo;

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<> (MainActivity.class);

    @Before
    public void setUp() throws Exception{
        solo = new Solo(InstrumentationRegistry.getInstrumentation(), activityTestRule.getActivity());
    }

    @Test
    public void setTextToActivity(){
        solo.assertCurrentActivity("CurrentActivity", MainActivity.class);
        solo.enterText(0, "30");
        solo.enterText(1, "2");
        solo.clickOnButton(4);
    }
}
